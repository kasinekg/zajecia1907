public class Tablice {

  public static void findHighestAndLowest(int[] tab) {
    int min = tab[0];
    int max = tab[0];

    for (int i = 0; i < tab.length; i++) {
      if (min > tab[i]) {
        min = tab[i];
      }
      if (max < tab[i]) {
        max = tab[i];
      }
    }
    System.out.println("Najmniejsza warto�� w tablicy to: " + min);
    System.out.println("Najwi�ksza warto�� w tablicy to: " + max);
  }

  public static void reverseArray(int[] tab) {
    System.out.println(Arrays.toString(tab));

    int start = 0;
    int end = tab.length - 1;
    while (start < end) {
      int temp = tab[start];
      tab[start] = tab[end];
      tab[end] = temp;

      start++;
      end--;
    }
    System.out.println(Arrays.toString(tab));
  }

  public static int mostFrequent(int[] tab) {
    int count = 0;
    int maxCount = 0;
    int id = 0;
    for (int i = 0; i < tab.length; i++) {
      for (int j = 0; j < tab.length; j++) {
        if (tab[i] == tab[j]) {
          count++;
        }
      }
      if (maxCount < count) {
        maxCount = count;
        id = i;
      }
      count = 0;
    }
    return tab[id];
  }

  public static int[] concatArrays(int[] tab1, int[] tab2) {
    if (tab1 == null) {
      return tab1;
    }
    if (tab2 == null) {
      return tab2;
    }

    int[] result = new int[tab1.length + tab2.length];
    for (int i = 0; i < tab1.length; i++) {
      result[i] = tab1[i];
    }
    for (int i = 0; i < tab2.length; i++) {
      result[i + tab1.length] = tab2[i];
    }
    return result;
  }

  public static int[] sortedArray(int[] tab) {
    System.out.println("Nieposortowana tablica: " + Arrays.toString(tab));
    int temp;
    for (int i = 0; i < tab.length - 1; i++) {
      for (int j = 0; j < tab.length - 1; j++) {
        if (tab[j] > tab[j + 1]) {
          temp = tab[j];
          tab[j] = tab[j + 1];
          tab[j + 1] = temp;
        }
      }
    }
    return tab;
  }
}
