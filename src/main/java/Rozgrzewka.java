import java.util.Scanner;

public class Rozgrzewka {

  public static void from1to100() {
    for (int i = 1; i<=100; i++ ) {
      System.out.println(i);
    }
  }

  public static void printInScope() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Podaj liczb� pocz�tkow�");
    int start = scanner.nextInt();
    System.out.println("Podaj liczb� ko�cz�c�");
    int stop = scanner.nextInt();
    scanner.close();
    for (int i = start; i <= stop; i++) {
      System.out.println(i);
    }
  }
  public static void printInScopeWithStep() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("Podaj liczb� pocz�tkow�");
    int start = scanner.nextInt();
    System.out.println("Podaj liczb� ko�cz�c�");
    int stop = scanner.nextInt();
    System.out.println("Podaj krok");
    int step = scanner.nextInt();
    scanner.close();
    for (int i = start; i <= stop; i = i+ step ) {
      System.out.println(i);
    }
  }
}
