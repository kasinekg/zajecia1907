public class Matematyka {
  public static void fibbonacci(int max) {
    int a1 = 1;
    int a2 = 1;
    for (int i = 0; i < max; i++) {
      if (i == 0) {
        System.out.print(0+ " ");
      }
      if(i < 2) {
        System.out.print(1+ " ");
      } else {
        int sum = a2 + a1;
        System.out.print(sum + " ");
        a2 = a1;
        a1 = sum;
      }
    }
  }

  public static int[] convertToString(int number) {
    String numberString = String.valueOf(number);
    int[] numTab = new int[numberString.length()];

    for (int i = 0; i < numberString.length(); i++) {
      String charStr = numberString.substring(i, i+1);
      numTab[i] = Integer.valueOf(charStr);
    }
    return numTab;
  }
}
